package getting.started

/**
 * All error object should be here. If your error message needs nothing but the message, implement it as `object`. For all
 * other cases, put a `case class` on top of that
 */
abstract class EntityError(msg: String)

object NoEntityFound extends EntityError("Entity not found")

object InvalidEntityFound extends EntityError("Invalid entity found")

object EmptyEntityFound extends EntityError("Empty entity found")

case class InProcessEntityError(id: Int) extends EntityError("Error happens while processing Entity#" + id)

object TrivialEntityError extends EntityError("You will never see me")