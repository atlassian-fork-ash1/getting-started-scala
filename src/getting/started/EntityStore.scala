package getting.started

import scala.util.Random

/**
 *
 * This class in charge of providing entity input. In order to getting started with random Entity, use `new Random().nextBoolean`
 * to determine the random state
 */
object EntityStore {

  def getEntity: Option[Entity] = ???

}
